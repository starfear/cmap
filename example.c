#include <stdio.h>
#include <stdlib.h>

#include "cmap/cmap.h"

char randchar()
{
	return 'A' + (rand() % 26);
}

int main()
{

	cmap *ll = malloc(sizeof(ll));
	cmap_init(ll);

	// pointer for future actions without deleting original
	cmap *l;

	// copy ptr to local pointer
	//l = ll;

	cmap_push(ll, "AAA", "AAA");
	cmap_push(ll, "BBB", "BBB");
	cmap_push(ll, "CCC", "CCC");
	cmap_push(ll, "DDD", "DDD");

	// create list for 10000 random elemenets
	// for(int i = 0; i < 10000; i++)
	// {
	// 	char key[8];
	// 	char* value = malloc(sizeof(char) * 3);
	// 	sprintf(key, "%c%c%c", randchar(), randchar(), randchar());
	// 	sprintf(value, "%c%c%c", randchar(), randchar(), randchar());
	// 	cmap_push(ll, key, value);
	// }

	// reset our local ptr
	l = ll;

	// GET value by key
	puts("\n\nTrying get value by key [DDD], must return DDD if yes.");
	char* val = cmap_get(ll, "DDD");
	if(val != NULL)
		puts(val);

	// Get index and value by key
	// will return [-1, NULL] if there are no such entries
	puts("\n\nTrying to get struct with index and value of entry by key.");
	struct cmap_iav a = cmap_get_iav(l, "AAA");
	printf("Index[%d] Value[%s] Valid[%d]\n", a.index, a.value, cmap_iav_is_valid(&a));

	// cmap_has
	puts("\n\nSome tests with cmap_has()");
	printf("Does our map has entry with key AAA? Answer: %d\n", cmap_has(ll, "AAA"));
	printf("Does our map has entry with key LOL? Answer: %d\n", cmap_has(ll, "LOL"));


	return 0;
	// ITERATIONS, here they go, comment line above.
	// simple iteration (obsolutely slow and useless...)
	puts("\n\nwhile i < len example");
	
	int len = cmap_length(ll);
	for(int i = 0; i <= len; i++)
	{
		printf("Key is %s, value is %s\n", l->key, l->value);
		l = l->next;
	}

	// reset our local ptr
	l = ll;

	// iterate while not NULL
	puts("\n\nwhile node != NULL example");
	while(l != NULL)
	{
		printf("Key is %s, value is %s\n", l->key, l->value);
		l = l->next;
	}

	// reset our local ptr
	l = ll;

	// even smarter while not
	puts("\n\nwhile node != NULL with index example");
	for(int i = 0; l != NULL; i++, l = l->next)
		printf("[%d] Key is %s, value is %s\n", i, l->key, l->value);


	// reset our local ptr
	l = ll;
	
	//but the best and easier way is to use foreach like that
	puts("\n\ncallback example");
	void callback(char* key, void* value)
	{
		printf("Key is %s, value is %s\n", key, value);
	}

	void callback_index(int index, char* key, void* value)
	{
		printf("Index is %d, key is %s, value is %s\n", index, key, value);
	}

	cmap_foreach(l, callback);
	cmap_foreach_index(l, callback_index);

}
