void cmap_init(cmap* node)
{
	memset(node->key, 0, KEY_LENGTH);
	node->value = NULL;
	node->next = NULL;
}

cmap* cmap_last(cmap* node)
{
	return node->next == NULL ? node : cmap_last(node->next);
}

int cmap_length(cmap* node)
{
	int i;
	for(i = 0; node->next != NULL; i++)
		node = node->next;
	return i;
}

void cmap_push(cmap* node, char *key, void *value)
{
	if(node->value == NULL && node->next == NULL)
	{
		strcpy(node->key, key);
		node->value = value;
	}
	else
	{
		cmap *n = malloc(sizeof(cmap));
		strcpy(n->key, key);
		n->value = value;
		cmap_last(node)->next = n;
	}
}

void cmap_foreach(cmap* node, void (*f)(char*, void*))
{
	while(node!= NULL)
	{
		f(node->key, node->value);
		node = node->next; 
	}
}

void cmap_foreach_index(cmap* node, void (*f)(int, char*, void*))
{
	for(int i = 0; node != NULL; i++, node = node->next)
		f(i, node->key, node->value);
}

void* cmap_get(cmap* node, char *key)
{
	while(node != NULL)
	{
		if(strcmp(node->key, key) == 0)
			return node->value;
		node = node->next; 
	}
	return NULL;	
}

struct cmap_iav cmap_get_iav(cmap* node, char *key)
{
	struct cmap_iav s;
	for(int i = 0; node != NULL; i++, node = node->next)
	{
		if(strcmp(node->key, key) == 0)
		{
			s.index = i;
			s.value = node->value;
			return s;
		}
	}
	s.index = -1;
	s.value = NULL;
	return s;
}

int cmap_iav_is_valid(struct cmap_iav* q)
{
	return q->index != -1;
}

void cmap_set(cmap* node, char* key, void* value)
{
	while(node != NULL)
	{
		if(strcmp(node->key, key) == 0)
		{
			node->value = value;
			return;
		}
		node = node->next; 
	}
}

int cmap_has(cmap* node, char* key)
{
	while(node != NULL)
	{
		if(strcmp(node->key, key) == 0)
		{
			return 1;
		}
		node = node->next; 
	}
	return 0;
}
