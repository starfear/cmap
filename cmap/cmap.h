#pragma once
#include <stdlib.h>
#include <string.h>

#define KEY_LENGTH 8

typedef struct CMAP_NODE_
{
	char  key[KEY_LENGTH];
	void* value;

	struct CMAP_NODE_ *next;
} cmap;

struct cmap_iav
{
	int   index;
	void* value;
};


// use it only to create first node, never next!
void cmap_init(cmap* node);
cmap* cmap_last(cmap* node);
void cmap_push(cmap* node, char* key, void* value);
int cmap_length(cmap* node);
void cmap_foreach(cmap* node, void (*f)(char*, void*));
void cmap_foreach_index(cmap* node, void (*f)(int, char*, void*));
void* cmap_get(cmap* node, char *key);
void cmap_set(cmap* node, char* key, void* value);

// cmap_has,
// return 1 if has
// return 0 if not
int cmap_has(cmap* node, char* key);

// returns struct cmap_iav with index and value by key
struct cmap_iav cmap_get_iav(cmap* node, char *key);
int cmap_iav_is_valid(struct cmap_iav* q);


#include "cmap.c"
