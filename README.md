# NAME
CMAP

#Description 
CMAP is a simple implementation of Map, based on Linked List for C Language;
Here you can find some useful functions to work with LinkedList structs.

## Usage
### Initializing and cleaning
```C
// initialize it
cmap ll;

// clear it
cmap_init(ll);
```

### Fill
```C
cmap_push(ll, "AAA", "AAA");
cmap_push(ll, "BBB", "BBB");
cmap_push(ll, "CCC", "CCC");
cmap_push(ll, "DDD", "DDD");
```

### Check if we have object with key "AAA" there
```C
printf("Does our map has entry with key AAA? Answer: %d\n", cmap_has(ll, "AAA"));
printf("Does our map has entry with key LOL? Answer: %d\n", cmap_has(ll, "LOL"));
```

### Get value by key
```C
char* val = cmap_get(ll, "AAA");
if(val != NULL) // check if it exists, cmap_get() must return NULL if not
	puts(val);
```

### Get index and value by Key
```C
/**
 * I use struct called as cmap_iav (CMAP INDEX AND VALUE) to response query.
 */

/* 	
struct cmap_iav
{
	int   index;
	void* value;
};
*/

struct cmap_iav a = cmap_get_iav(l, "AAA");
printf("Index[%d] Value[%s] Valid[%d]\n", a.index, a.value, cmap_iav_is_valid(&a));

/**
 * cmap_get_iav always returns struct cmap_iav, if it can't find entry by given key
 * it still returns struct, but index is -1 and value is NULL
 */
```

### Foreach to Callback
I thought about iteration and created this simple way to iterate list. Take a look.
```C
void callback(char* key, void* value)
{
	printf("Key is %s, value is %s\n", key, value);
}

void callback_index(int index, char* key, void* value)
{
	printf("Index is %d, key is %s, value is %s\n", index, key, value);
}

cmap_foreach(l, callback);
cmap_foreach_index(l, callback_index);
```

.. you can find other helpful functions in cmap/cmap.h

## Support
You can submit an issue.

## License
gpl-3.0
